﻿using System;
using System.IO;
using System.Windows.Forms;
using SudokuSolver.Logic;

namespace SudokuSolver
{
    public partial class SudokuSolver : Form
    {
        private readonly Solver _solver = new();
        private readonly Validator _validator = new();
        
        private readonly TextBox[,] _sudokuTextBoxes;
        private readonly TextBox _inputTextBox;

        private readonly string _testData =
            "x,x,x,x,x,x,x,x,x" + Environment.NewLine +
            "x,1,2,x,3,4,5,6,7" + Environment.NewLine +
            "x,3,4,5,x,6,1,8,2" + Environment.NewLine +
            "x,x,1,x,5,8,2,x,6" + Environment.NewLine +
            "x,x,8,6,x,x,x,x,1" + Environment.NewLine +
            "x,2,x,x,x,7,x,5,x" + Environment.NewLine +
            "x,x,3,7,x,5,x,2,8" + Environment.NewLine +
            "x,8,x,x,6,x,7,x,x" + Environment.NewLine +
            "2,x,7,x,8,3,6,1,5";

        public SudokuSolver()
        {
            InitializeComponent();
            
            var sudokuLayoutPanel = InitializeSudokuGrid();
            _inputTextBox = InitializeInputTextBox();
            _sudokuTextBoxes = InitializeSudokuTextResults(sudokuLayoutPanel);
            
            InitializeButtons();
        }

        private TableLayoutPanel InitializeSudokuGrid()
        {
            var sudokuLayoutPanel = new TableLayoutPanel
            {
                RowCount = 9,
                ColumnCount = 9,
                Dock = DockStyle.Right,
                Size = ClientSize with { Width = 250 }
            };
            
            Controls.Add(sudokuLayoutPanel);
            return sudokuLayoutPanel;
        }
        
        private TextBox InitializeInputTextBox()
        {
            var inputTextBox = new TextBox
            {
                Multiline = true,
                Dock = DockStyle.Left,
                Anchor = AnchorStyles.Left,
                Width = 150,
                Height = 150,
                Text = _testData,
                ReadOnly = false,
            };
            
            Controls.Add(inputTextBox);
            return inputTextBox;
        }

        private TextBox[,] InitializeSudokuTextResults(TableLayoutPanel tableLayoutPanel)
        {
            var sudokuTextBoxes = new TextBox[9, 9];
            for (var i = 0; i < 9; i++)
            {
                for (var j = 0; j < 9; j++)
                {
                    sudokuTextBoxes[i, j] = new TextBox
                    {
                        MaxLength = 1,
                        Dock = DockStyle.Left,
                        TextAlign = HorizontalAlignment.Center,
                        ReadOnly = true,
                        Width = 20
                    };
                    tableLayoutPanel.Controls.Add(sudokuTextBoxes[i, j], j, i);
                }
            }
            return sudokuTextBoxes;
        }

        private void InitializeButtons()
        {
            InitializeButton("Solve", SolveButton_Click);
            InitializeButton("Clear", ClearButton_Click);
            InitializeButton("Save", SaveButton_Click);
            InitializeButton("Load", LoadButton_Click);
        }

        private void InitializeButton(string name, EventHandler buttonOnClick)
        {
            var newButton = new Button
            {
                Text = name,
                Dock = DockStyle.Bottom
            };
            newButton.Click += buttonOnClick;
            Controls.Add(newButton);
        }

        private void SolveButton_Click(object sender, EventArgs e)
        {
            var board = ParseInput(_inputTextBox.Text);
            if (board == null)
            {
                MessageBox.Show(@"Parse error!");
                return;
            }
            
            if (!_validator.IsValidSudoku(board))
            {
                MessageBox.Show(@"Invalid Sudoku!");
                return;
            }

            if (_solver.SolveSudoku(board))
            {
                UpdateSudokuTextBoxes(board);
            }
            else
            {
                MessageBox.Show(@"No solution found!");
            }
        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            _inputTextBox.Clear();
            ClearSudokuTextBoxes();
        }
        
        private void UpdateSudokuTextBoxes(int[,] board)
        {
            for (var i = 0; i < 9; i++)
            {
                for (var j = 0; j < 9; j++)
                {
                    _sudokuTextBoxes[i, j].Text = board[i, j] == 0 ? "" : board[i, j].ToString();
                }
            }
        }

        private void ClearSudokuTextBoxes()
        {
            for (var i = 0; i < 9; i++)
            {
                for (var j = 0; j < 9; j++)
                {
                    _sudokuTextBoxes[i, j].Clear();
                }
            }
        }

        private static int[,]? ParseInput(string input)
        {
            var lines = input.Split([Environment.NewLine], StringSplitOptions.RemoveEmptyEntries);
            if (lines.Length != 9)
                return null;

            var board = new int[9, 9];
            for (var i = 0; i < 9; i++)
            {
                var numbers = lines[i].Split(',');
                if (numbers.Length != 9)
                    return null;

                for (var j = 0; j < 9; j++)
                {
                    if (numbers[j] == "x")
                    {
                        board[i, j] = 0;
                    }
                    else
                    {
                        if (!int.TryParse(numbers[j], out var value) || value < 0 || value > 9)
                            return null;

                        board[i, j] = value;
                    }
                }
            }

            return board;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            using var saveFileDialog = new SaveFileDialog();
            
            saveFileDialog.Filter = @"Text Files (*.txt)|*.txt";
            
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(saveFileDialog.FileName, _inputTextBox.Text);
                MessageBox.Show(@"File saved successfully!");
            }
        }

        private void LoadButton_Click(object sender, EventArgs e)
        {
            using var openFileDialog = new OpenFileDialog();
            
            openFileDialog.Filter = @"Text Files (*.txt)|*.txt";
            
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                _inputTextBox.Text = File.ReadAllText(openFileDialog.FileName);
                MessageBox.Show(@"File loaded successfully!");
            }
        }
    }
}