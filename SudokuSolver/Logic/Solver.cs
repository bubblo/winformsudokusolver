using System;
using System.Collections.Generic;

namespace SudokuSolver.Logic;

public class Solver
{
    public bool SolveSudoku(int[,] board)
    {
        var nextCell = GetNextCell(board);
        if (nextCell == null)
        {
            return true; 
        }

        var (row, col) = (nextCell.Item1, nextCell.Item2);
        for (var num = 1; num <= 9; num++)
        {
            if (!IsPossible(board, row, col, num)) continue;
                
            board[row, col] = num;
            if (SolveSudoku(board))
            {
                return true;
            }

            board[row, col] = 0;
        }

        return false;
    }
    
    private Tuple<int, int>? GetNextCell(int[,] board)
    {
        var minOptions = 10;
        Tuple<int, int>? bestCell = null;

        for (var row = 0; row < 9; row++)
        {
            for (var col = 0; col < 9; col++)
            {
                if (board[row, col] == 0)
                {
                    var options = GetPossibleValues(board, row, col).Count;
                    if (options < minOptions)
                    {
                        minOptions = options;
                        bestCell = new Tuple<int, int>(row, col);
                    }
                }
            }
        }

        return bestCell;
    }

    private List<int> GetPossibleValues(int[,] board, int row, int col)
    {
        var possibleValues = new List<int>();
        for (var num = 1; num <= 9; num++)
        {
            if (IsPossible(board, row, col, num))
            {
                possibleValues.Add(num);
            }
        }

        return possibleValues;
    }
    
    private bool IsPossible(int[,] board, int row, int col, int num)
    {
        for (var i = 0; i < 9; i++)
        {
            var againIncludeIn3X3 = board[row - row % 3 + i / 3, col - col % 3 + i % 3] == num;
            if (board[row, i] == num || board[i, col] == num || againIncludeIn3X3)
            {
                return false;
            }
        }

        return true;
    }
}