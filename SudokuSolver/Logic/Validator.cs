using System.Collections.Generic;

namespace SudokuSolver.Logic;

public class Validator
{
    public bool IsValidSudoku(int[,] board)
    {
        for (int i = 0; i < 9; i++)
        {
            if (!IsValidRow(board, i) || !IsValidColumn(board, i) || !IsValidBlock(board, i / 3 * 3, i % 3 * 3))
                return false;
        }

        return true;
    }

    private bool IsValidRow(int[,] board, int row)
    {
        var set = new HashSet<int>();
        for (var i = 0; i < 9; i++)
        {
            if (board[row, i] != 0 && !set.Add(board[row, i]))
                return false;
        }

        return true;
    }

    private bool IsValidColumn(int[,] board, int col)
    {
        var set = new HashSet<int>();
        for (var i = 0; i < 9; i++)
        {
            if (board[i, col] != 0 && !set.Add(board[i, col]))
                return false;
        }

        return true;
    }

    private bool IsValidBlock(int[,] board, int startRow, int startCol)
    {
        var set = new HashSet<int>();
        for (var i = startRow; i < startRow + 3; i++)
        {
            for (var j = startCol; j < startCol + 3; j++)
            {
                if (board[i, j] != 0 && !set.Add(board[i, j]))
                    return false;
            }
        }

        return true;
    }
}